(function () {
    'use strict';

    var _templateBase = './scripts';


    var app = angular.module(
        'app',
        [
            'ngRoute',
            'ngMaterial',
            'ngAnimate'
        ]
    );
    app.config(['$routeProvider',
          function ($routeProvider) {
              $routeProvider.when('/', {
                  templateUrl: _templateBase+'home/home.html',
                  controller: 'HomeCtrl',
                  controllerAs: '_ctrl'
              });
              $routeProvider.otherwise({redirectTo: '/'});
          }
      ]);

    //for tabular stuff
    angular.module('navBarDemoBasicUsage', ['ngMaterial'])
        .controller('AppCtrl', AppCtrl);

    function AppCtrl($scope) {
      $scope.currentNavItem = 'page1';
    }

})();
