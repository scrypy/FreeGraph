# FreeGraph
Free form network style graph generation. Ever wanted to mess around with
graph theory? Or perhaps you want to chart a particular set of data by
hand? With FreeGraph double click the canvas to create a new node, and
select two different nodes to create a link between them.
---

## Future Plans
====
* Saving/Loading Graphs
* Click to arrange based on different algorithms
