//modded from Ben Guo’s Block 4370043
//http://bl.ocks.org/benzguo/4370043

///////////////////// d3 setup /////////////////////
var width = $('#chart').width(),
    height = 780,
    fill = d3.scale.category20();

var nodeR = 20,
    linkL = 100;

var COUNTER = 0;

var CURRENT_NODE=null;

// mouse event vars
var selected_node = null,
    selected_link = null,
    mousedown_link = null,
    mousedown_node = null,
    mouseup_node = null;

// init svg
var outer = d3.select("#chart")
  .append("svg:svg")
    .attr("width", width)
    .attr("height", height)
    .attr("pointer-events", "all");

var vis = outer
  .append('svg:g')
    .call(d3.behavior.zoom().on("zoom", rescale))
    .on("dblclick.zoom", null)
  .append('svg:g')
    .on("mousemove", mousemove)
    .on("mousedown", mousedown)
    .on("mouseup", mouseup);

vis.append('svg:rect')
    .attr('width', width)
    .attr('height', height)
    .attr('fill', 'white');

///////////////////// init force layout /////////////////////

var paz = document.location.pathname;
var pazz= paz.split('/');
var GRAPH_NAME=pazz[2];

force = d3.layout.force()
    .size([width, height])
    .nodes([{id:COUNTER,name:COUNTER}]) // initialize with a single node
    .linkDistance(linkL)
    .charge(-200)
    .on("tick", tick);
COUNTER++;
// line displayed when dragging new nodes
var drag_line = vis.append("line")
    .attr("class", "drag_line")
    .attr("x1", 0)
    .attr("y1", 0)
    .attr("x2", 0)
    .attr("y2", 0);

// get layout properties
var nodes = force.nodes(),
    links = force.links(),
    node = vis.selectAll(".node"),
    link = vis.selectAll(".link");

// add keyboard callback
d3.select(window)
    .on("keydown", keydown);

redraw();

///////////////////// load graph /////////////////////

if(GRAPH_NAME=='new'){
  //make new graph
  console.log('new graph - no load');
  $.ajax({
    async:false,
    global:false,
    type: 'GET',
    contentType: 'application/json',
    url: '/newgraph',
    success: function(data) {
        console.log('[NEWGRAPH] SUCC');
    },
    error: function(msg){
      console.log('[NEWGRAPH] ERR\n'+msg);
    }
  });
}else{
  //load graph data
  var graphData=function(){
    var tmp = null;
    $.ajax({
      async:false,
      global:false,
      type: 'GET',
      contentType: 'application/json',
      url: '/getgraph/'+GRAPH_NAME,
      success: function(data) {
          console.log('success-ajax');
          //console.log(JSON.stringify(data));
          tmp = data;
      },
      error: function(msg){
        console.log('fail-ajax');
        tmp = {"code":"bad","msg":"AJAX-FAIL"};
      }
    });
    return tmp;
  }();
  console.log(JSON.stringify(graphData));
  if(graphData.code=='good'){
    console.log('doing good graph load');
    nodes = graphData.data.nodes;
    links = graphData.data.links;
    //resetting counter variable on existing graph load
    for(var i=0;i<nodes.length;i++){
      if(nodes[i].id>COUNTER){
        COUNTER=nodes[i].id;
      }
    }
    redraw();
  }else{
    console.log('doing bad graph load'+graphData.code);
  }
}


function mousedown() {
  if (!mousedown_node && !mousedown_link) {
    // allow panning if nothing is selected
    vis.call(d3.behavior.zoom().on("zoom"), rescale);
    return;
  }
}

function mousemove() {
  if (!mousedown_node) return;
  // update drag line
  drag_line
      .attr("x1", mousedown_node.x)
      .attr("y1", mousedown_node.y)
      .attr("x2", d3.svg.mouse(this)[0])
      .attr("y2", d3.svg.mouse(this)[1]);
}

function mouseup() {
  if (mousedown_node) {
    // hide drag line
    drag_line
      .attr("class", "drag_line_hidden")

    if (!mouseup_node) {
      // add node
      var point = d3.mouse(this),
        node = {id:COUNTER, name:COUNTER, x: point[0], y: point[1]},
        n = nodes.push(node);
      COUNTER++;
      // select new node
      selected_node = node;
      selected_link = null;

      // add link to mousedown node
      links.push({source: mousedown_node, target: node});

      //create new notes file for node
      notesFile({"type":"create","graph":GRAPH_NAME,"node":node});
    }
    redraw();
  }else{
    //node clicked, now open notes
    showNote(selected_node);//from note-cntrl
  }
  // clear mouse event vars
  resetMouseVars();
}

function resetMouseVars() {
  mousedown_node = null;
  mouseup_node = null;
  mousedown_link = null;
}

function tick() {
  link.attr("x1", function(d) { return d.source.x; })
      .attr("y1", function(d) { return d.source.y; })
      .attr("x2", function(d) { return d.target.x; })
      .attr("y2", function(d) { return d.target.y; });

  node.attr("cx", function(d) { return d.x; })
      .attr("cy", function(d) { return d.y; });
}

// rescale g
function rescale() {
  trans=d3.event.translate;
  scale=d3.event.scale;

  vis.attr("transform",
      "translate(" + trans + ")"
      + " scale(" + scale + ")");
}

// redraw force layout
function redraw() {
  link = link.data(links);
  link.enter()
      .insert("line", ".node")
      .attr("class", "link")
      .on("mousedown",
          function(d) {
            mousedown_link = d;
            if (mousedown_link == selected_link) selected_link = null;
            else selected_link = mousedown_link;
            selected_node = null;
            redraw();
          }
        );
  link.exit().remove();
  link.classed("link_selected", function(d) { return d === selected_link; });

  node = node.data(nodes);
  node.enter().insert("circle")
      .attr("class", "node")
      .attr("r", 10)
      .on("mousedown",
        function(d) {
          // disable zoom
          vis.call(d3.behavior.zoom().on("zoom"), null);

          mousedown_node = d;
          if (mousedown_node == selected_node) selected_node = null;
          else selected_node = mousedown_node;
          selected_link = null;

          // reposition drag line
          drag_line
              .attr("class", "link")
              .attr("x1", mousedown_node.x)
              .attr("y1", mousedown_node.y)
              .attr("x2", mousedown_node.x)
              .attr("y2", mousedown_node.y);

          redraw();
        })
      .on("mousedrag",
        function(d) {
          // redraw();
        })
      .on("mouseup",
        function(d) {
          if (mousedown_node) {
            mouseup_node = d;
            if (mouseup_node == mousedown_node) { resetMouseVars(); return; }

            // add link
            var link = {source: mousedown_node, target: mouseup_node};
            links.push(link);

            // select new link
            selected_link = link;
            selected_node = null;

            // enable zoom
            vis.call(d3.behavior.zoom().on("zoom"), rescale);
            redraw();
          }
        })
      //.on("click",function(d){clickNode(d)})
      .transition()
        .duration(750)
        .ease("elastic")
        .attr("r", nodeR);

  //trying to make a title for nodes
  // NOT WORKING YET
  node.append("text")
      .attr("dx", 12)
      .attr("dy", ".35em")
      .text(function(d){return d.name;});

  node.exit().transition()
      .attr("r", 0)
      .remove();

  node.classed("node_selected", function(d) { return d === selected_node; });

  if (d3.event) {
    // prevent browser's default behavior
    d3.event.preventDefault();
  }
  force.start();
}

function spliceLinksForNode(node) {
  toSplice = links.filter(
    function(l) {
      return (l.source === node) || (l.target === node); });
  toSplice.map(
    function(l) {
      links.splice(links.indexOf(l), 1); });
}

function keydown() {
  if (!selected_node && !selected_link) return;
  switch (d3.event.keyCode) {
    case 8: // backspace
      break; //deletes the node when we are editing the name
    case 46: { // delete
      if (selected_node) {
        nodes.splice(nodes.indexOf(selected_node), 1);
        spliceLinksForNode(selected_node);
        notesFile({"type":"delete","graph":GRAPH_NAME,"node":selected_node});//will hopefully work
      }else if (selected_link) {
        links.splice(links.indexOf(selected_link), 1);
      }
      selected_link = null;
      selected_node = null;
      redraw();
      break;
    }
  }
}

//function for opening node to view info
/*   TO REMOVE
function clickNode(nd){//use other function, this one sucks
  console.log('clik node 4 info');
  CURRENT_NODE == nd ? CURRENT_NODE=null : CURRENT_NODE=nd;
}
*/
function editNodeModal(){
  if(selected_node==null){//no node selected, show warning
    showToast('No node selected');
  }else{//show edit node modal
    $('#editNodeModal').modal();

    var form = document.getElementById('EditNodeForm');
    form.elements.namedItem('NodeIndex').value=selected_node.id;
    if(typeof selected_node.name === 'undefined'){//name not yet set
      form.elements.namedItem('NodeName').placeholder="<Enter name>";
    }else{
      form.elements.namedItem('NodeName').value=selected_node.name;
    }
  }
}

function saveGraphModal(){
  $('#saveGraphModal').modal('toggle');
  var form = document.getElementById('SaveGraphForm');
  var ur = document.location.pathname.split('/');
  if(ur[2]=='new'){
    form.elements.namedItem('SaveGraphFileName').placeholder="<Enter name>";
  }else{
    form.elements.namedItem('SaveGraphFileName').value=ur[2].split('.')[0];
  }
}

function saveGraph(){
  graph={};
  _g={};
  formData = $('#SaveGraphForm').serializeArray();
  //console.log(JSON.stringify(formData));
  graph.nodes=nodes;
  graph.links=links;
  _g.filename_before=GRAPH_NAME;
  _g.filename_after=formData[0].value;
  _g.graph=graph;

  //console.log(JSON.stringify(_g));
  $.ajax({
		type: 'POST',
		data: JSON.stringify(_g),
    contentType: 'application/json',
    url: '/savegraph',
    success: function(data) {
      console.log('[SAVEGRAPH] SUCCESS');
      console.log(JSON.stringify(data));
      $('#saveGraphModal').modal('toggle');
      GRAPH_NAME=_g.filename_after;
      showToast(_g.filename_after+' Saved');
    },
    error: function(data){
      console.log('[SAVEGRAPH] ERR');
      console.log(JSON.stringify(data));
    }
  });

}

// save changes from editNodeModal
function saveNode(){
  var form = document.getElementById('EditNodeForm');
  var nodeName = form.elements.namedItem('NodeName').value
  selected_node.name=nodeName;
  $('#editNodeModal').modal('toggle');
  showToast('Node ['+nodeName+'] Saved');
}

// save file associated with node
function saveFile(){
  formData = $('#EditNodeForm').serializeArray();
  console.log(JSON.stringify(formData));

  $.ajax({
		type: 'POST',
		data: JSON.stringify(_g),
    contentType: 'application/json',
    url: '/savefile',
    success: function(data) {
      console.log('[SAVEFILE] SUCCESS');
      console.log(JSON.stringify(data));
    },
    error: function(data){
      console.log('[SAVEFILE] ERR');
      console.log(JSON.stringify(data));
    }
  });
}

// create/delete markdown notes file associated w/ node
// input: {"type":"create|delete","graph":<graphname>,"node":<node>}
function notesFile(obj){
  $.ajax({
		type: 'POST',
		data: JSON.stringify(obj),
    contentType: 'application/json',
    url: '/notesfile',
    success: function(data) {
        console.log('[NOTESFILE] SUCCESS');
        console.log(JSON.stringify(data));
    },
    error: function(data){
      console.log('[NOTESFILE] ERR');
      console.log(JSON.stringify(data));
    }
  });
}


///////////////////// MISC FUNCTS  /////////////////////

function showToast(msg){
  var x = document.getElementById("snackbar");
  if(x.hasChildNodes()){
    x.removeChild(x.childNodes[0]);
  }
  x.appendChild(document.createTextNode(msg));
  x.className = "show";
  // After 3 seconds, remove the show class from DIV
  setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
}
