// home page scripts
//graphs={};

var graphs=function(){
  var tmp = null;
  $.ajax({
    async:false,
    global:false,
    type: 'GET',
    contentType: 'application/json',
    url: '/graphs',
    success: function(data) {
        console.log('success');
        //console.log(JSON.stringify(data));
        tmp = data;
    },
    error: function(msg){
      console.log('fail');
      console.log(JSON.stringify(data));
    }
  });
  return tmp;
}();

console.log(JSON.stringify(graphs));

//generate table
for(var i=0;i<graphs.length;i++){
  var row = document.createElement('tr');
  var nameTD = document.createElement('td');
  var nameLink=document.createElement('a');
  var nameTXT = document.createTextNode(graphs[i].name);
  var sizeTD = document.createElement('td');
  var sizeTXT = document.createTextNode(graphs[i].size+" MB");//FIX to show relative scale
  nameLink.appendChild(nameTXT);
  nameLink.href='/wiki/'+graphs[i].name;
  nameTD.appendChild(nameLink);
  sizeTD.appendChild(sizeTXT);
  row.appendChild(nameTD);
  row.appendChild(sizeTD);
  document.getElementsByTagName('tbody')[0].appendChild(row);
}
