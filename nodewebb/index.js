/*
ROUTING
-web serv and routing
*/
var express = require('express');
var app = express();
var fs = require('fs');
var path = require('path');
var jsonfile = require('jsonfile');
var bodyParser = require('body-parser');

var memy = require('./settings.json');

//use public directory to store files that will be used in browser
app.use(express.static(path.join(__dirname+'/public')));

//set template engine to use pug(formerly jade)
app.set('view engine','pug');

app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use( bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));

//reason for node web app is for pdf support
/// this is spotty in electron apparantly

// *** HOME PAGE *** //
//get home page
app.get('/', function (req, res) {
  var graphs = fs.readdirSync(path.join(__dirname,'graphs'));
  console.log('g='+graphs);
  res.sendFile(path.join(__dirname+'/public/index.html'));
});

//make new graph directory
app.get('/newgraph',function(req,res){
  var newDirName = path.join(__dirname,memy.GRAPH_SAVE_LOCATION,'new');
  //check if new graph dir exists
  if(fs.existsSync(newDirName)){
    //remove everything inside
    recursiveDelete(newDirName);
  }
  //make new graph dir
  fs.mkdirSync(newDirName);
  fs.mkdirSync(path.join(newDirName,memy.NOTES_SAVE_LOCATION));
  fs.mkdirSync(path.join(newDirName,memy.FILE_SAVE_LOCATION));
  console.log('**newgraph done**');
  res.send('[NEWGRAPH]-SUCC');
});

//return list of available graph files
app.get('/graphs', function(req,res){
  var retObj = [];
  var graphfiles = fs.readdirSync(path.join(__dirname,memy.GRAPH_SAVE_LOCATION));
  for(var i=0;i<graphfiles.length;i++){
    retObj.push({name:graphfiles[i],
                  "size":(fs.statSync(path.join(__dirname+'/public/graphs/',graphfiles[i])).size/ 1000000.0)});
  }
  res.json(retObj);
});

// *** WIKI PAGE *** //
//display wiki page
app.get('/wiki/:graphID', function (req, res) {
  res.sendFile(path.join(__dirname+'/public/wiki2.html'));
});

//load graph data for wiki page
app.get('/getgraph/:graphID', function (req, res) {
  console.log(req.params.graphID);
  if(req.params.graphID != 'new'){
    var file = req.params.graphID;
    var graphData = jsonfile.readFileSync(path.join(__dirname+'/public/graphs/',file,'/graph.json'));
    if(graphData != null){
      //return graph data
      res.json({"code":"good","data":graphData});
    }else{
      //read graph file error
      res.json({"code":"bad","msg":"READ-GRAPH-FILE"});
    }
  }else{
    //send error, requested new graph
    res.json({"code":"bad","msg":"REQ-NEW-GRAPH"});
  }
});

//save a graph
app.post('/savegraph', function(req,res){
  var graphPath = path.join(__dirname,memy.GRAPH_SAVE_LOCATION,req.body.filename_before);
  console.log(graphPath);
  if(fs.existsSync(graphPath)){
    console.log('path exists');
    var graphPathAfter = path.join(__dirname,memy.GRAPH_SAVE_LOCATION,req.body.filename_after);
    fs.renameSync(graphPath,graphPathAfter);
    file = path.join(graphPathAfter,'graph.json');
  }else{
    //shouldn't be reached
    console.log('path !exists');
    fs.mkdirSync(graphPath);
    file = path.join(graphPath,'graph.json');
  }
  console.log('filename='+file);
  //console.log('req=='+JSON.stringify(req.body));

  jsonfile.writeFile(file, req.body.graph, function (err) {
    console.error(err);
  });
  res.send('successfull save');
});

//copy file into files directory
app.post('/savefile', function(req,res){
  var file = path.join(__dirname,memy.GRAPH_SAVE_LOCATION,req.body.filename+'.json');
  //console.log('filename='+file);
  //console.log('req=='+JSON.stringify(req.body));

  jsonfile.writeFile(file, req.body.graph, function (err) {
    console.error(err);
  });
  res.send('successfull save');
});

// create/delete markdown notes file associated w/ node
app.post('/notesfile', function(req,res){
  var file = path.join(__dirname,
                      memy.GRAPH_SAVE_LOCATION,
                      req.body.graph,
                      memy.NOTES_SAVE_LOCATION,
                      req.body.node.id+'.md');
  console.log('[NOTESFILE]-FILE= '+file);
  if(req.body.type == "create"){
    // makefile
    fs.closeSync(fs.openSync(file,'w'));
  }else{
    // deletefile
    fs.stat(file,function(err,stats){
      if(err){return console.error('[NOTESFILE]'+file+'DEL ERR-stat\n'+err);}
      fs.unlink(file,function(err){
        if(err){return console.error('[NOTESFILE]'+file+'DEL ERR-unlink\n'+err);}
        console.log('[NOTESFILE]'+file+'DEL SUCCESS');
      })
    });
  }
  //console.log('filename='+file);
  //console.log('req=='+JSON.stringify(req.body));

  res.send('successfull save');
});

//start the app
app.listen(8080, function () {
  console.log('FreeGraph wiki listening on port 8080!');
});


//https://stackoverflow.com/questions/18052762/remove-directory-which-is-not-empty
var recursiveDelete = function(path){
  if( fs.existsSync(path) ) {
    fs.readdirSync(path).forEach(function(file,index){
      var curPath = path + "/" + file;
      if(fs.lstatSync(curPath).isDirectory()) { // recurse
        recursiveDelete(curPath);
      } else { // delete file
        fs.unlinkSync(curPath);
      }
    });
    fs.rmdirSync(path);
  }
}
