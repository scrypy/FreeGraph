Short-Term
* link nodes with notes
* copy pdf into <graph>/files when associated with node
* Edit Markdown files
- Options
-- Editor.md
-- stackedit.io
-- dillinger.io
-- simplemde

Long-Term
* Save graphs/files to Github (for now jus CLI it)
* Change default save locations
* Implementing search functions and statistics on graphs
-- such as breadth/depth first search, travel salesman?
* Click to arrange based on different algorithms
-- currently only force-directed, need to research other options
   with d3.js
