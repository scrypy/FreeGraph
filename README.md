# FreeGraph
FreeGraph is a graph based wiki where, instead of links on pages like a normal wiki, the links are represented by a force directed graph. This allows for a more visual representation of relations between the subjects in your wiki. The current working version is located in the nodewebb directory.

## How to Use
Download into the directory of your choosing. Navigate to that directory and then go to the nodewebb/ directory. Use <code>npm install</code> to install all necessary node modules and then
<code>node index.js</code> to begin the web server.
Go to your browser and point towards localhost:8080. From the home page either select and existing With FreeGraph. From the wiki page, click and drag from any node to create a new node, then select any node to add/edit/view a file you want associated with that node.

By default all files associated with a node will be copied to the /public/files/ directory and all graphs default save location is in /public/graphs/ .



## Future Plans
* Save graphs/files to Github (for now jus CLI it)
* Edit Markdown files
* Change default save locations
* Implementing search functions and statistics on graphs
* Click to arrange based on different algorithms

Plans to have support for PDF, Markdown, photos/screenshots, and text files. The goal is to have anything renderable and showable in the browser as a possibility for node content.


I am open to any suggestions for improvements or new features, just create a new issue here with your recommendations.
